﻿using UnityEngine;

public abstract class TileGenerator 
{
    protected int m_Seed;
    public TileGenerator(int seed)
    {
        this.m_Seed = seed;
    }
    public abstract float GenerateTileValue(int x, int y, int width, int height);
}
public class RandomTileGenerator : TileGenerator
{
    public RandomTileGenerator(int seed) : base(seed)
    {
        Random.InitState(seed);
    }
    public override float GenerateTileValue(int x, int y, int width, int height)
    {
        return Random.value;
    }
}
public class PerlinNoiseTileGenerator : TileGenerator
{
    private float m_Scale;
    public PerlinNoiseTileGenerator(int seed, float scale) : base(seed)
    {
        this.m_Scale = scale;
    }
    public override float GenerateTileValue(int x, int y, int width, int height)
    {
        Random.InitState(m_Seed);
        float xCoordinate = (float)x / width;
        float yCoordinate = (float)y / height;
        return Mathf.PerlinNoise(Random.Range(0,100) + xCoordinate * m_Scale, Random.Range(0, 100) + yCoordinate * m_Scale);
        
    }
}
