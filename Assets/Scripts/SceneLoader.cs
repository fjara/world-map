﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour
{
    private static SceneLoader m_Instance;
   
    private MapType m_MapType = MapType.Invalid;
    void Awake()
    {
        m_Instance = this;
    }

    void Update()
    {
#if UNITY_ANDROID
        if (Input.GetKeyDown(KeyCode.Escape) && !IsGameSceneActive())
        {
            Application.Quit();
        }
#endif
    }

    public static SceneLoader GetInstance()
    {
        return m_Instance;
    }

    public void OnArchipelagoMapButtonClicked()
    {
        m_MapType = MapType.Archipelago;
        LoadGame();

    }

    public void OnLakeMapButtonClicked()
    {
        m_MapType = MapType.Lake;
        LoadGame();
    }

    public void OnDesertMapButtonClicked()
    {
        m_MapType = MapType.Desert;
        LoadGame();
    }

    public MapType GetMapType()
    {
        return m_MapType;
    }

    public void LoadGame()
    {
        SceneManager.LoadScene(1, LoadSceneMode.Single);
    }

    public void LoadMainMenu()
    {
        SceneManager.LoadScene(0, LoadSceneMode.Single);
    }

    public void OnExitButtonClicked()
    {
        Application.Quit();
    }

    public bool IsGameSceneActive()
    {
        return SceneManager.GetActiveScene().name.Equals("Game");
    }
}
