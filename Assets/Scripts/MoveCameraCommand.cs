﻿using UnityEngine;

public abstract class MoveCameraCommand
{
    private float m_MovementAmount;

    public MoveCameraCommand(float movementAmount)
    {
        m_MovementAmount = movementAmount;
    }

    public Vector3 execute(Camera camera, Vector3 cameraPosition, MapInfo mapInfo, float cameraZoom)
    {
        float height = 2f * camera.orthographicSize;
        float width = height * camera.aspect;
        float deltaMovement = m_MovementAmount * cameraZoom * Time.deltaTime;
        return moveCamera(cameraPosition, mapInfo, deltaMovement, width, height);
    }

    public abstract Vector3 moveCamera(Vector3 cameraPosition, MapInfo mapInfo, float deltaMovement, float width, float height);

}

public class MoveCameraRightCommand : MoveCameraCommand
{
    public MoveCameraRightCommand(float movementAmount) : base(movementAmount)
    {
    }
    public override Vector3 moveCamera(Vector3 cameraPosition, MapInfo mapInfo, float deltaMovement, float width, float height)
    {
        cameraPosition.x += deltaMovement;
        return cameraPosition;
    }
}

public class MoveCameraLeftCommand : MoveCameraCommand
{
    public MoveCameraLeftCommand(float movementAmount) : base(movementAmount)
    {
    }

    public override Vector3 moveCamera(Vector3 cameraPosition, MapInfo mapInfo, float deltaMovement, float width, float height)
    {
        cameraPosition.x -= deltaMovement;
        return cameraPosition;
    }
}

public class MoveCameraUpCommand : MoveCameraCommand
{
    public MoveCameraUpCommand(float movementAmount) : base(movementAmount)
    {
    }

    public override Vector3 moveCamera(Vector3 cameraPosition, MapInfo mapInfo, float deltaMovement, float width, float height)
    {
        cameraPosition.y += deltaMovement;
        return cameraPosition;
    }
}

public class MoveCameraDownCommand : MoveCameraCommand
{
    public MoveCameraDownCommand(float movementAmount) : base(movementAmount)
    {
    }

    public override Vector3 moveCamera(Vector3 cameraPosition, MapInfo mapInfo, float deltaMovement, float width, float height)
    {
        cameraPosition.y -= deltaMovement;
        return cameraPosition;
    }
}