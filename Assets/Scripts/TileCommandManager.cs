﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class TileCommandManager
{
    private Stack m_CommandStack = new Stack();

    public void ExecuteCommand(DestroyTileCommand command, Tilemap tilemap, Vector3Int tilePosition, TileBase newTile, List<List<int>> worldMap, MapInfo mapInfo)
    {
        command.Execute(tilemap, tilePosition, newTile, worldMap, mapInfo);
        m_CommandStack.Push(command);
    }


    public bool Undo(Tilemap tilemap, List<List<int>> worldMap, MapInfo mapInfo)
    {
        if (m_CommandStack.Count > 0)
        {
            DestroyTileCommand command = (DestroyTileCommand)m_CommandStack.Pop();
            command.Undo(tilemap, worldMap, mapInfo);
            return true;
        }
        return false;
    }
}

public abstract class DestroyTileCommand
{
    protected TileBase m_DestroyedTile;
    protected TileType m_DestroyedTileType;
    protected Vector3Int m_DestroyedTilePosition;
    public abstract void Execute(Tilemap tilemap, Vector3Int tilePosition, TileBase newTile, List<List<int>> worldMap, MapInfo mapInfo);
    public abstract void Undo(Tilemap tilemap, List<List<int>> worldMap, MapInfo mapInfo);
}

public class DestroyHouseTileCommand : DestroyTileCommand
{
    public override void Execute(Tilemap tilemap, Vector3Int tilePosition, TileBase newTile, List<List<int>> worldMap, MapInfo mapInfo)
    {
        m_DestroyedTile = tilemap.GetTile(tilePosition);
        m_DestroyedTilePosition = tilePosition;
        m_DestroyedTileType =(TileType) worldMap[tilePosition.x + mapInfo.map_width / 2][tilePosition.y + mapInfo.map_height / 2];
       
        tilemap.SetTile(tilePosition, newTile);
        worldMap[tilePosition.x + mapInfo.map_width / 2][tilePosition.y + mapInfo.map_height / 2] = (int)TileType.Sand;

    }

    public override void Undo(Tilemap tilemap, List<List<int>> worldMap, MapInfo mapInfo)
    {
        tilemap.SetTile(m_DestroyedTilePosition, m_DestroyedTile);
        worldMap[m_DestroyedTilePosition.x + mapInfo.map_width / 2][m_DestroyedTilePosition.y + mapInfo.map_height / 2] = (int)m_DestroyedTileType;

    }
}