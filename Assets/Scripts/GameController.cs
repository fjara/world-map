﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.Tilemaps;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class GameController : MonoBehaviour
{
    public GameObject m_ExplosionPrefab;

    public CameraManager m_CameraManager;
    public HudManager m_HudManager;
    public MapManager m_MapManager;
    public NetworkManager m_NetworkManager;

    private const float EdgeMouseSensitivity = 10f;
    private const string Url = "https://gist.githubusercontent.com/anonymous/63de7fecde7289804f95619c9d20c7ad/raw/a6e24694cfbfef42fbb018283b5e570173a2e816/map.json";
            
    private MapInfo m_MapInfo;
    private bool m_IsMapGenerated = false;
    private Vector3Int m_SelectedHouse;
    private GameObject m_ExplosionObject;
    private Dictionary<string, string> m_TilesToTilesName;

    private Vector2 m_StartPosition;
    private Vector2 m_DragStartPosition;
    private Vector2 m_DragNewPosition;
    private Vector2 m_Finger0Position;
    private float m_DistanceBetweenFingers;
    private bool m_IsZooming;

    void Start()
    {
        m_NetworkManager.Setup(OnMapDataFetched);
        m_NetworkManager.GetMapInfo(Url);

        m_HudManager.SetActiveLoadingCircle(true);
    }
    void Update()
    {
        if (m_IsMapGenerated)
        {
#if UNITY_ANDROID
            HandleMapScrollAndZoomOnAndroid();
#endif
#if UNITY_STANDALONE || UNITY_EDITOR
            HandleMapZoom();
            HandleMapScrolling();
#endif
            HandleTilesInput();
            DrawVisibleTilesOnMap();

            if (m_CameraManager.HasCameraChanged())
            {
                m_HudManager.ShowHidePopUp(false);
            }
        }

#if UNITY_ANDROID
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            OnBack();
        }
#endif
    }

    public void OnMapDataFetched(MapInfo mapInfo)
    {
        m_MapInfo = mapInfo;
        MapTilesToTileNames();
        GenerateMap();
    }

    private void MapTilesToTileNames()
    {
        m_TilesToTilesName = new Dictionary<string, string>();
        for(int i = 0; i < m_MapInfo.tiles.Count; i++)
        {
            m_TilesToTilesName[m_MapInfo.tiles[i].type] = m_MapInfo.tiles[i].name;
        }
    }

    public void GenerateMap()
    {
        m_MapManager.GenerateMap(m_MapInfo);
        m_IsMapGenerated = true;

        m_HudManager.UpdateNumberOfHouses(m_MapInfo.number_of_houses);
        m_HudManager.SetActiveLoadingCircle(false);
    }

    private void HandleMapScrolling()
    {
        if (Input.GetKey(KeyCode.W) || Input.mousePosition.y > Screen.height - EdgeMouseSensitivity)
        {
            m_CameraManager.MoveCamera(Dirrection.Up, m_MapInfo);
        }

        if (Input.GetKey(KeyCode.S) || Input.mousePosition.y < EdgeMouseSensitivity)
        {
            m_CameraManager.MoveCamera(Dirrection.Down, m_MapInfo);
        }

        if (Input.GetKey(KeyCode.D) || Input.mousePosition.x > Screen.width - EdgeMouseSensitivity)
        {
            m_CameraManager.MoveCamera(Dirrection.Right, m_MapInfo);
        }

        if (Input.GetKey(KeyCode.A) || Input.mousePosition.x < EdgeMouseSensitivity)
        {
            m_CameraManager.MoveCamera(Dirrection.Left, m_MapInfo);
        }

        m_CameraManager.AdjustCamera(m_MapInfo);
    }
    private void HandleMapZoom()
    {
        if(Input.GetKey(KeyCode.KeypadPlus) || Input.mouseScrollDelta.y > 0)
        {
            m_CameraManager.AdjustZoom(-1);
            m_HudManager.ShowHidePopUp(false);
        }

        if (Input.GetKey(KeyCode.KeypadMinus) || Input.mouseScrollDelta.y < 0)
        {
            m_CameraManager.AdjustZoom(1);
            m_HudManager.ShowHidePopUp(false);
        }

        m_CameraManager.AdjustCamera(m_MapInfo);
    }


    private void HandleMapScrollAndZoomOnAndroid()
    {
        if (Input.touchCount == 0 && m_IsZooming)
        {
            m_IsZooming = false;
        }

        if (Input.touchCount == 1)
        {
            if (!m_IsZooming)
            {
                if (Input.GetTouch(0).phase == TouchPhase.Moved)
                {
                    Vector2 NewPosition = m_CameraManager.GetWorldPosition();
                    Vector2 PositionDifference = NewPosition - m_StartPosition;
                    m_CameraManager.TranslateCamera(-PositionDifference);
                    m_CameraManager.AdjustCamera(m_MapInfo);
                }
                m_StartPosition = m_CameraManager.GetWorldPosition();
            }
        }
        else if (Input.touchCount == 2)
        {
            if (Input.GetTouch(1).phase == TouchPhase.Moved)
            {
                m_IsZooming = true;

                m_DragNewPosition = m_CameraManager.GetWorldPositionOfFinger(1);
                Vector2 PositionDifference = m_DragNewPosition - m_DragStartPosition;

                if (Vector2.Distance(m_DragNewPosition, m_Finger0Position) < m_DistanceBetweenFingers)
                {
                    m_CameraManager.AdjustZoom(1);
                    m_CameraManager.AdjustCamera(m_MapInfo);
                    m_HudManager.ShowHidePopUp(false);
                }

                if (Vector2.Distance(m_DragNewPosition, m_Finger0Position) >= m_DistanceBetweenFingers)
                {
                    m_CameraManager.AdjustZoom(-1);
                    m_CameraManager.AdjustCamera(m_MapInfo);
                    m_HudManager.ShowHidePopUp(false);
                }
                m_DistanceBetweenFingers = Vector2.Distance(m_DragNewPosition, m_Finger0Position);
            }
            m_DragStartPosition = m_CameraManager.GetWorldPositionOfFinger(1);
            m_Finger0Position = m_CameraManager.GetWorldPositionOfFinger(0);
        }
    }

    private void HandleTilesInput()
    {
        if (Input.GetMouseButtonUp(0) && !EventSystem.current.IsPointerOverGameObject())
        {
            Vector3 position = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            Vector3 mousePosition = new UnityEngine.Vector3(position.x, position.y, 0);
            OnTileClicked(mousePosition);
        }
    }

    private void OnTileClicked( Vector3 mousePosition)
    {
        m_HudManager.DisplayPopUpOnTile(m_TilesToTilesName[m_MapManager.GetTileUnderMouseName(mousePosition)], m_MapManager.IsSelectedTileHouse(mousePosition));

        if (m_MapManager.IsSelectedTileHouse(mousePosition))
        {
            m_SelectedHouse = m_MapManager.GetSelectedHousePosition(mousePosition);
        }
    }

    public void DrawVisibleTilesOnMap()
    {
        Vector2 cameraSize = m_CameraManager.GetCameraSize();
        m_MapManager.DrawMap(m_MapInfo, m_CameraManager.GetCameraPosition(), cameraSize.x, cameraSize.y);
    }

    public void OnDestroyHouse()
    {
        m_HudManager.UpdateNumberOfHouses(--m_MapInfo.number_of_houses);
        m_HudManager.ShowHidePopUp(false);

        m_MapManager.DestroyHouseOnMap(m_SelectedHouse, m_MapInfo);
        
        Destroy(m_ExplosionObject);
        m_ExplosionObject = Instantiate(m_ExplosionPrefab, m_SelectedHouse + new Vector3(0.5f,0.5f,0), Quaternion.identity);
    }

    public void OnUndoDestroyHouse()
    {
        m_HudManager.ShowHidePopUp(false);

        if(m_MapManager.UndoDestroyHouseOnMap(m_MapInfo))
        {
            m_HudManager.UpdateNumberOfHouses(++m_MapInfo.number_of_houses);
        }
    }

    public void OnBack()
    {
        m_IsMapGenerated = false;
        m_MapManager.UnloadTiles(m_MapInfo);
        SceneLoader.GetInstance().LoadMainMenu();
    }
}
