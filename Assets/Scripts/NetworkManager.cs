﻿using UnityEngine.Networking;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class TileInfo
{
    public string type;
    public string name;
    public int level;
}

[Serializable]
public class MapInfo
{
    public int map_width;
    public int map_height;
    public int number_of_houses;
    public List<TileInfo> tiles;

    public static MapInfo CreateFromJSON(string jsonString)
    {
        return JsonUtility.FromJson<MapInfo>(jsonString);
    }
}

public class NetworkManager : MonoBehaviour
{
    private Action<MapInfo> OnMapDataFetched;

    public void Setup(Action<MapInfo> OnMapDataFetched)
    {
        this.OnMapDataFetched = OnMapDataFetched;
    }
    public void GetMapInfo(string url)
    {
        StartCoroutine(GetRequest(url));
    }
    IEnumerator GetRequest(string url) 
    {
        using (UnityWebRequest webRequest = UnityWebRequest.Get(url))
        {
            yield return webRequest.SendWebRequest();

            if (webRequest.isNetworkError)
            {
                UnityEngine.Debug.Log("Network Error");
            }
            else
            {
                OnMapDataFetched(MapInfo.CreateFromJSON(webRequest.downloadHandler.text));
            }
        }
    }
}
