﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HudManager : MonoBehaviour
{
    public GameObject m_PopUpPanel;
    public RectTransform m_PopUpRectTransform;
    public Text m_PopUpText;
    public GameObject m_PopUpButton;
    public Text m_NumberOfHouses;
    public GameObject m_LoadingCircle;

    void Start()
    {
        ShowHidePopUp(false);
    }

    public void ShowHidePopUp(bool value)
    {
        m_PopUpPanel.SetActive(value);
    }

    public void DisplayPopUpOnTile(string textToDisplay, bool shouldDisplayButton)
    {
        m_PopUpPanel.transform.position = Input.mousePosition + new Vector3(m_PopUpRectTransform.sizeDelta.x / 2, m_PopUpRectTransform.sizeDelta.y / 2, 0);
        m_PopUpText.text = textToDisplay;
        m_PopUpButton.SetActive(shouldDisplayButton);
        ShowHidePopUp(true);
    }

    public void UpdateNumberOfHouses(int numberOfHouses)
    {
        m_NumberOfHouses.text = numberOfHouses + "";
    }

    public void SetActiveLoadingCircle(bool active)
    {
        m_LoadingCircle.SetActive(active);
    }
}
