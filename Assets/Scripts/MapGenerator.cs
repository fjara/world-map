﻿using System.Collections.Generic;
using UnityEngine;

public class MapGenerator
{
    private TileGenerator m_TileGenerator;
    private TileClassifier m_TileClassifier;

    public MapGenerator(TileGenerator tileGenerator, TileClassifier tileCalculator)
    {
        this.m_TileGenerator = tileGenerator;
        this.m_TileClassifier = tileCalculator;
    }
    public List<List<int>> GenerateMap(MapInfo mapInfo, int tileTypes)
    {
        List<List<int>> tiles = new List<List<int>>();

        for (int i = 0; i < mapInfo.map_width; i++)
        {
            List<int> row = new List<int>();
            for (int j = 0; j < mapInfo.map_height; j++)
            {
                float generatedTile = m_TileGenerator.GenerateTileValue(i, j, mapInfo.map_width, mapInfo.map_height);
                int classifiedTile = (int)m_TileClassifier.ClassifyTile(generatedTile);
                row.Add(classifiedTile);
            }
            tiles.Add(row);
        }

        int attemptLimit = mapInfo.map_width * mapInfo.map_height;
        int numberOfHousesLeftToGenerate = mapInfo.number_of_houses;
        Random.InitState(4);
        while (numberOfHousesLeftToGenerate > 0 && attemptLimit > 0)
        {
            int x = Random.Range(0, mapInfo.map_width);
            int y = Random.Range(0, mapInfo.map_height);
            if (tiles[x][y] == 2)
            {
                tiles[x][y] = 5 + Random.Range(0,2);
                numberOfHousesLeftToGenerate--;
            }
            attemptLimit--;
        }
        mapInfo.number_of_houses -= numberOfHousesLeftToGenerate;

        return tiles;
    }
}
