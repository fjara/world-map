﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class TileClassifier
{
    protected float[] m_Weights;
    protected float[] m_Thresholds = new float[6];

    public TileClassifier()
    {
        InitializeWeights();
        CalculateThresholds();
    }
    private void CalculateThresholds()
    {
        float sum = 0;
        for (int i = 0; i < m_Weights.Length; i++)
        {
            sum += m_Weights[i];
        }
        m_Thresholds[0] = m_Weights[0] / sum;

        for (int i = 1; i < m_Weights.Length; i++)
        {
            m_Thresholds[i] = m_Thresholds[i - 1] + m_Weights[i] / sum;
        }
    }

    public TileType ClassifyTile(float value)
    {
        if (value < m_Thresholds[(int)TileType.Water])
        {
            return TileType.Water;
        }
        else if (value < m_Thresholds[(int)TileType.Sand])
        {
            return TileType.Sand;
        }
        else if (value < m_Thresholds[(int)TileType.Grass])
        {
            return TileType.Grass;
        }
        else if (value < m_Thresholds[(int)TileType.Tree])
        {
            return TileType.Tree;
        }
        else if (value < m_Thresholds[(int)TileType.Forest])
        {
            return TileType.Forest;
        }
        else
        {
            return TileType.Water;
        }
    }
    protected abstract void InitializeWeights();

}

public class LakeMapTileClassifier : TileClassifier
{   protected override void InitializeWeights()
    {
        m_Weights = new float[] { 2, 1, 2, 1, 3 ,0};
    }
}

public class ArchipelagoMapTileClassifier : TileClassifier
{   protected override void InitializeWeights()
    {
        m_Weights = new float[] { 5, 1, 2, 1, 1 ,0};
    }
}

public class DesertMapTileClassifier : TileClassifier
{
    protected override void InitializeWeights()
    {
        m_Weights = new float[] { 0,7, 1, 1, 0, 2};
    }
   
}


