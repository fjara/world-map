﻿using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.Tilemaps;

public enum MapType
{
    Lake,
    Archipelago,
    Desert,
    Count,
    Invalid
}
public enum TileType
{
    Water,
    Sand,
    Grass,
    Tree,
    Forest,
    Barrack,
    Storage,
    Count,
    Invalid
}
public class MapManager : MonoBehaviour
{   
    public Tilemap m_TileMap;
    public Tile[] m_Tiles;
    public Grid m_TileGrid;
    
    private TileCommandManager m_TileCommandManager = new TileCommandManager();

    private List<List<int>> m_WorldMap;
    private MapGenerator m_MapGenerator;

    void Start()
    {
        MapType mapType = SceneLoader.GetInstance().GetMapType();
        TileClassifier tileClassifier;
        switch(mapType)
        {
            case MapType.Desert:
                tileClassifier = new DesertMapTileClassifier();
                break;
            case MapType.Archipelago:
                tileClassifier = new ArchipelagoMapTileClassifier();
                break;
            default:
                tileClassifier = new LakeMapTileClassifier();
                break;
        }
        
        m_MapGenerator = new MapGenerator(new PerlinNoiseTileGenerator((int)System.DateTime.Now.Ticks, 10), tileClassifier);
    }

    public void GenerateMap(MapInfo mapInfo)
    {
        m_WorldMap = m_MapGenerator.GenerateMap(mapInfo, 8);
    }

    public void DrawMap(MapInfo mapInfo, Vector3 cameraPosition, float cameraWidth, float cameraHeight )
    {
        int startX = mapInfo.map_width / 2;
        int startY = mapInfo.map_height / 2;
        for (int i = (int)Math.Floor(cameraPosition.x - cameraWidth / 2); i < cameraPosition.x + (int)Math.Ceiling(cameraWidth / 2); i++)
        {
            for (int j = (int)Math.Floor(cameraPosition.y - cameraHeight / 2); j < cameraPosition.y + (int)Math.Ceiling(cameraHeight / 2); j++)
            {
                if (startX + i < 0 || startX + i >= mapInfo.map_width || startY + j < 0 || startY + j >= mapInfo.map_height)
                {
                    continue;
                }

                Vector3Int tilePosition = new Vector3Int(i, j, 0);
                m_TileMap.SetTile(tilePosition, m_Tiles[m_WorldMap[startX + i][startY + j]]);
            }
        }
    }

    public void DestroyHouseOnMap(Vector3Int houseToDestroy, MapInfo mapInfo)
    {
        m_TileCommandManager.ExecuteCommand(new DestroyHouseTileCommand(), m_TileMap, houseToDestroy, m_Tiles[(int)TileType.Sand], m_WorldMap, mapInfo);
    }

    public bool UndoDestroyHouseOnMap(MapInfo mapInfo)
    {
        return m_TileCommandManager.Undo( m_TileMap,  m_WorldMap, mapInfo);
    }

    public TileBase GetTileUnderMouse(Vector3 mousePosition)
    {
        Vector3Int mouseCell = m_TileGrid.WorldToCell(mousePosition);
        return m_TileMap.GetTile(mouseCell);
    }

    public Vector3Int GetSelectedHousePosition(Vector3 mousePosition)
    {
        return m_TileGrid.WorldToCell(mousePosition);
    }

    public bool IsSelectedTileHouse(Vector3 mousePosition)
    {
        Vector3Int mouseCell = m_TileGrid.WorldToCell(mousePosition);
        TileBase tileUnderMouse = m_TileMap.GetTile(mouseCell);
        if (tileUnderMouse == m_Tiles[(int)TileType.Barrack] || tileUnderMouse == m_Tiles[(int)TileType.Storage])
        {
            return true;
        }
        return false;
    }

    public string GetTileUnderMouseName(Vector3 mousePosition)
    {
        Vector3Int mouseCell = m_TileGrid.WorldToCell(mousePosition);
        TileBase tileUnderMouse = m_TileMap.GetTile(mouseCell);
        string tileName = "";
        if(tileUnderMouse != null)
        {
            tileName = tileUnderMouse.name;
        }
        return tileName;
    }

    public void UnloadTiles(MapInfo mapInfo)
    {
        for(int i = 0 - mapInfo.map_width/2;  i < mapInfo.map_width/2; i++ )
        {
            for (int j = 0 - mapInfo.map_height / 2; j < mapInfo.map_height / 2; j++)
            {
                m_TileMap.SetTile(new Vector3Int(i, j, 0), null);
            }
        }
    }
}
