﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Dirrection
{
    Up,
    Down,
    Left,
    Right,
    Count,
    Invalid
}

public class CameraManager : MonoBehaviour
{
    private const float MinimumZoom = 5f;
    private const float MaximumZoom = 20f;
    private const float CameraMovementAmount = 3f;
    private const float CameraZoomSpeed = 3f;

    private Camera m_Camera;
    private MoveCameraCommand[] m_CameraCommands;

    private Vector3 m_CameraPosition = new Vector3(0, 0, -10);
    private float m_CameraZoom = 5;

    private void Start()
    {
        m_Camera = transform.GetComponent<Camera>();
        transform.position = m_CameraPosition;
        m_CameraCommands = new MoveCameraCommand[(int)Dirrection.Count];
        m_CameraCommands[(int)Dirrection.Up] = new MoveCameraUpCommand(CameraMovementAmount);
        m_CameraCommands[(int)Dirrection.Down] = new MoveCameraDownCommand(CameraMovementAmount);
        m_CameraCommands[(int)Dirrection.Left] = new MoveCameraLeftCommand(CameraMovementAmount);
        m_CameraCommands[(int)Dirrection.Right] = new MoveCameraRightCommand(CameraMovementAmount);
    }

    void Update()
    {
        HandleMovement();
        HandleZoom();
    }

    private void HandleMovement()
    {
        m_CameraPosition.z = transform.position.z;
        transform.position = m_CameraPosition;
    }

    private void HandleZoom()
    {
        float cameraDifference = m_CameraZoom - m_Camera.orthographicSize;
        m_Camera.orthographicSize += cameraDifference * CameraZoomSpeed * Time.deltaTime;
    }

    public void MoveCamera(Dirrection dirrection, MapInfo mapInfo)
    {
        m_CameraPosition = m_CameraCommands[(int)dirrection].execute(m_Camera, m_CameraPosition, mapInfo, m_CameraZoom);
    }

    public void TranslateCamera(Vector2 translateAmount)
    {
        m_Camera.transform.Translate(translateAmount);
        m_CameraPosition = m_Camera.transform.position;
    }

    public void AdjustCamera(MapInfo mapInfo)
    {
        float height = 2f * m_Camera.orthographicSize;
        float width = height * m_Camera.aspect;

        if (0 - height / 2 + m_CameraPosition.y < (0 - mapInfo.map_height / 2))
        {
            m_CameraPosition.y = 0 - mapInfo.map_height / 2 + height / 2;
        }

        if (height / 2 + m_CameraPosition.y > mapInfo.map_height / 2)
        {
            m_CameraPosition.y = mapInfo.map_height / 2 - height / 2;
        }

        if (0 - width / 2 + m_CameraPosition.x < (0 - mapInfo.map_width / 2))
        {
            m_CameraPosition.x = 0 - mapInfo.map_width / 2 + width / 2;
        }

        if (width / 2 + m_CameraPosition.x > mapInfo.map_width / 2)
        {
            m_CameraPosition.x = mapInfo.map_width / 2 - width / 2;
        }
    }

    public void AdjustZoom(int zoomAmount)
    {
        m_CameraZoom += zoomAmount;
        m_CameraZoom = Mathf.Clamp(m_CameraZoom, MinimumZoom, MaximumZoom);
    }

    public Vector3 GetCameraPosition()
    {
        return transform.position;
    }

    public bool HasCameraChanged()
    {
        bool returnValue = m_Camera.transform.hasChanged;
        m_Camera.transform.hasChanged = false;
        return returnValue;
    }

    public Vector2 GetCameraSize()
    {
        float height = 2f * m_Camera.orthographicSize;
        float width = height * m_Camera.aspect;
        return new Vector2(width, height);
    }

    public Vector2 GetWorldPosition()
    {
        return m_Camera.ScreenToWorldPoint(Input.mousePosition);
    }

    public Vector2 GetWorldPositionOfFinger(int FingerIndex)
    {
        return m_Camera.ScreenToWorldPoint(Input.GetTouch(FingerIndex).position);
    }
}
