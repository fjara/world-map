When running in the Editor mode, add and run the scene MainMenu.

PC (Windows)
- Scroll the map using keys w,a,s,d or moving the mouse to the edge of the screen.
- Zoom the map using Num+ and Num- or using mouse wheel.

Android
- Scroll the map by swiping in any direction
- Zoom the map using two finger pinch.
- Go back or exit the game using UI buttons or using Android Back Button.